# Searx-Qt
Desktop client application for Searx/SearXNG/searx-space.

![screenshot](https://notabug.org/CYBERDEViL/searx-qt/raw/master/docs/images/screenshot_0_4a1.png)

## About

Search with `Searx`/`SearXNG` instances without the need for a complex
web-browser.

Searx-Qt is a desktop application that lets you search on
public `SearXNG` instances listed on `https://searx.space` (or you
can add `Searx` or `SearXNG` instance urls manually).

The aim of the project is to preform search operations on `Searx`/`SearXNG`
instances with the use of their API and without the need for a complex
web-browser. `Searx-Qt` will do the bare minimum of what is
required to search, no cookies and no javascript. Just render a
`Searx`/`SearXNG` API request to something usefull.

Unfortunately most public `SearXNG` instances actively block requests to
their `JSON-API`. *But don't worry!* Thats why there is a option in the
`SearXNG settings` (which is on by default) to parse `HTML` instead, and
still no javascript and no cookies.

For more detailed information about Searx-Qt see `docs/index.rst` or
`docs/index.html`.


### Note

The `searx-space` project no longer lists `Searx` instances (since
1 Sept. 2023) as the `Searx` project is no longer maintained. The 'new'
`Searx` is a fork called `SearXNG`, only these instances will be listed by
the `searx-space` project.

Most of the public `SearXNG` instances listed by `searx-space` don't allow
the use of the API, they will see `Searx-Qt` as a bot and block the request,
only a few public instances remain that don't have this strict policy.

What you can do about? Well you have the following options:

 - Setup your own `Searx`/`SearXNG` instance and add it manually to
   `Searx-Qt`.
 - Add a few remaining working `Searx` instances manually.
 - Use the `Parse HTML` option under `SearXNG` settings.


### Searx / SearXNG

Actual search requests will be made to a server running the `Searx`/`SearXNG`
software, there are many public available. We call such servers 'instances'.

#### `Searx`

The `Searx` project is no longer maintained but for now `Searx-Qt`
will still support it.

 - API Docs: https://searx.github.io/searx/dev/search_api.html
 - Source: https://github.com/searx/searx
 - License: AGPL3
 - Status: Inactive

#### `SearXNG`

 - API Docs: https://docs.searxng.org/dev/search_api.html
 - Source: https://github.com/searxng/searxng
 - License: AGPL3
 - Status: Active

### searx-space

The `searx-space` project lists public `SearXNG` instances with statistics.
The official instance is running at `https://searx.space/`. This is where
`Searx-Qt` will request a list with instances when the update button is
pressed by default (the `searx-space` url can be changed in the settings).

- Source: https://github.com/searxng/searx-space
- License: AGPL3

___

## License

GPL3 (https://www.gnu.org/licenses/gpl-3.0.en.html)

___

## Contact

You may want to open an issue [here](https://notabug.org/CYBERDEViL/searx-qt/issues)
or you can check on IRC (`#searx-qt` on `irc.libera.chat:6697`).

___

## Dependecies

| name | version | license | url |
| :--  | :------ | :------ | :-- |
| Python | 3 | PSFL | https://docs.python.org/3/license.html |
| requests | | Apache 2 | http://docs.python-requests.org/en/master/ |
| jsonschema | >= 4.10 | MIT | https://github.com/python-jsonschema/jsonschema |
| PyQt5 | | GPL3 | https://www.riverbankcomputing.com/software/pyqt/intro |
| urllib3 | | MIT | https://urllib3.readthedocs.io/ |
| beautifulsoup4 | | MIT | https://www.crummy.com/software/BeautifulSoup/ |


#### * Optional

| name | version | license | url | desc |
| :--  | :------ | :------ | :-- | :--- |
| pysocks |  | BSD | https://github.com/Anorov/PySocks | For socks proxy support |
| pillow |  | BSD | https://python-pillow.org | For thumbnail support |


#### Building

| name | version | license | url | desc |
| :--  | :-----  | :------ | :-- | :--- |
| gettext | - | GPL | https://www.gnu.org/software/gettext/ | Compiling translations. |
| PyQt5-Dev-Tools | - | Qt5 | GPL3 | https://riverbankcomputing.com/software/pyqt/intro | Compiling theme icons |


#### Packaging

| name | version | license | url | desc |
| :--  | :-----  | :------ | :-- | :--- |
| python-build | - | MIT | https://github.com/pypa/build | Building Python package
| python-installer | - | MIT | https://github.com/pypa/installer | Building Python package
| python-wheel | - | MIT | https://pypi.python.org/pypi/wheel | Building Python package
| python-setuptools | - | PSF | https://pypi.org/project/setuptools/ | Building Python package
