#!/bin/bash
# requires: python-docutils

# Get into the project root
cd $(dirname "${BASH_SOURCE}"); cd ..

rst2html --title="Searx-Qt documentation" \
  --stylesheet=docs/style.css docs/index.rst \
  docs/index.html
