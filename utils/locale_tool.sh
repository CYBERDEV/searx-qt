#!/bin/bash
# requires: gettext

# Get into the project root
cd $(dirname "${BASH_SOURCE}"); cd ..

LANGUAGES=("all")
for d in locale/*/ ; do
    [ -d "$d" ] || continue
    LANGUAGES+=($(basename "$d"))
done


function createTemplate ( ) {
    local VERSION=$(python -c 'from searxqt.version import __version__; print(__version__)')
    local CHARSET='UTF-8'

    if [ -f locale/searx-qt.pot ]; then
        rm locale/searx-qt.pot
    fi
    touch locale/searx-qt.pot
    find ./searxqt/ -type f -name '*.py' -exec xgettext --join-existing \
     --from-code=utf-8 -L python -d 'searx-qt' -o locale/searx-qt.pot {} \;
    #/usr/lib/python3.8/Tools/i18n/pygettext.py -d searx-qt -o locale/searx-qt.pot ./searxqt/

    replaceInPOT "PACKAGE VERSION" "$VERSION"
    replaceInPOT "CHARSET" "$CHARSET"
}


function replaceInPOT ( ) {
    sed -i -e "0,/$1/ s/$1/$2/" locale/searx-qt.pot
}


function updateLanguage ( ) {
    local language="$1"
    echo -n "$language: "
    msgmerge -N "locale/$language/LC_MESSAGES/searx-qt.po" locale/searx-qt.pot > "locale/$language/LC_MESSAGES/new.po"
    # TODO update the version in the header
    rm "locale/$language/LC_MESSAGES/searx-qt.po"
    mv "locale/$language/LC_MESSAGES/new.po" "locale/$language/LC_MESSAGES/searx-qt.po"
}


function updateAllLanguages ( ) {
    for langCode in ${LANGUAGES[@]:1} ; do
        updateLanguage "$langCode"
    done
}


function compileLanguage ( ) {
    local language="$1"
    echo -n "$language: "
    msgfmt -o "locale/$language/LC_MESSAGES/searx-qt.mo" "locale/$language/LC_MESSAGES/searx-qt"
    echo "done."
}


function compileAllLanguages ( ) {
    for langCode in ${LANGUAGES[@]:1} ; do
        compileLanguage "$langCode"
    done
}


function requireValidLanguageArg ( ) {
    if [ ! $# -eq 2 ]; then
        echo "Please supply a valid second argument"
        exit 2
    fi

    if [[ ! " ${LANGUAGES[@]} " =~ " ${2} " ]]; then
        echo "Language '$2' not found."
        exit 2
    fi
}


HELPTEXT='Valid options:

  -c --create-template
    Create searx-qt.pot

  -u --update-language LANGUAGE
    Update the .po file of a specific LANGUAGE.
    Set LANGUAGE to "all" to update all languages.

  -m --compile-language LANGUAGE
    Compile .mo file for a specific LANGUAGE.
    Set LANGUAGE to "all" to compile all languages.

  -l --list
    Prints all available LANGUAGEs.
'


option="$1"
case $option in
    -c|--create-template)
        echo "[Create tempalte]"
        createTemplate
        echo "Done."
        ;;

    -u|--update-language)
        requireValidLanguageArg $@
        echo "[Update language] $2"
        if [ "$2" == "all" ]; then
            updateAllLanguages
        else
            updateLanguage "$2"
        fi
        ;;

    -m|--compile-language)
        requireValidLanguageArg $@
        echo "[Compile language] $2"
        if [ "$2" == "all" ]; then
            compileAllLanguages
        else
            compileLanguage "$2"
        fi
        ;;

    -l|--list)
        echo "Supported languages:"
        for langCode in ${LANGUAGES[@]:1}; do
            echo "  $langCode"
        done
    ;;

    *)
        echo "$HELPTEXT"
        exit 1
esac

exit 0
