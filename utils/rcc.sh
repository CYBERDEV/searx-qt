#!/bin/bash
# qt's rcc need to run in the same directory as the .qrc file, that's why this
# script.

# Get into the project root
cd $(dirname "${BASH_SOURCE}"); cd ..

ROOT=$1  # theme root (relative to project root)
QRC=$2  # theme it's .qrc file
RCC="${QRC%.*}.rcc"  # the output .rcc file

# Get into the theme root
cd "$ROOT"

# Compile the resource
rcc --binary "$QRC" -o "$RCC"
exit $?
