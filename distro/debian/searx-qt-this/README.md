# Build a Debian package from this git repo.

Checkout whatever branch/tag/commit you like before running `makedeb.sh`.

Usage:

```
../makedeb.sh
```
