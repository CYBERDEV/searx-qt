# CHANGELOG

For a full overview of all changes see
https://notabug.org/CYBERDEViL/searx-qt

## Version 0.6.0-alpha4

 * [FIX] Searx-space schema.


## Version 0.6.0-alpha3

 * [FIX] Searx-space schema.


## Version 0.6.0-alpha2

 * [FIX] Fix fetching instances from Searx-space (updated json
         validation schema).
 * [FIX] Open links in custom browser (it was blocking Searx-Qt).


## Version 0.6.0-alpha1

 * [NEW] Settable command to handle links (like a webbrower or torrent client).
 * [NEW] Max receive size and chunk size settings.
 * [UPT] Distutils has been replaced by Setuptools and a Makefile.
 * [UPT] Debian package creation script.
 * [UPT] Small schema adjustment.
 * [UPT] Searx-Qt will now do `POST` requests instead of `GET` requests for
         search requests.
 * [FIX] Avoid being blocked.
 * [FIX] Crash in HTML parser.


## Version 0.5.0-alpha1

 * [NEW] SearXNG settings:
         - Option to parse HTML instead of using the JSON API, since most
           public SearXNG instances actively block API requests.
         - Safe Search option for search request (and engines that support it).
 * [NEW] Connection settings: extra request headers.
 * [NEW] Show all the engines of a search result instead of one.
 * [UPT] The "Web" profile preset will be selected as default instead of
         "None".
 * [UPT] Enable Guard by default.
 * [FIX] Schema fixes.


## Version 0.4.0-alpha1

  **NOTE**: This version aims to support both SearX and SearXNG.

 * [NEW] Use JSON Schema (python-jsonschema) for verification of received json
         data instead of the custom implementation.
 * [NEW] InstancesTable: Copy any column value(s) to clipboard of selected
         instance(s).
 * [NEW] InstancesTable: Copy JSON data to clipboard of selected instance(s).
 * [NEW] SearXNG instance version string support.
 * [NEW] Display search errors per instance with fallback enabled when all
         sampled instances failed.
 * [NEW] Simplified proxy settings, it is no longer possible to set a separate
         proxy for HTTP and HTTPS requests, the proxy settings will from now
         apply to both.
 * [UPT] Removed Qt stuff from the core package.
 * [FIX] Resample (10) fallback instances after stop button pressed.
 * [FIX] Deleting profile(s), it would crash in some cases before.
 * [FIX] Clear search results on profile switch.


## Version 0.3-beta3

 * [FIX] Updating instances (stats2 API update?)


## Version 0.3-beta2

 * [FIX] Loading default profile.


## Version 0.3-beta1

  **NOTE**: Settings/profiles from older versions of Searx-Qt will not work
  because those versions store non-mutable objects and are hard to migrate.
  Therefore you should delete your old config directory (possible at
  `~/.config/CYBERDEViL/searx-qt`, depending on your GNU/Linux distro).
  For future versions of Searx-Qt config migration code should be no
  problem to implement.

 * [NEW] Guard; put failing instances on timeout or the blacklist based on
         settable rules. This is optional.
 * [NEW] Settable style and theme.
 * [NEW] Search language favorites.
 * [NEW] Extra info for search results (suggestions, corrections,
         unresponsive_engines, infoboxes and answers).
 * [NEW] Show image format in search results when available.
 * [NEW] Ability to collapse categories and engines labels.
 * [NEW] Option from context-menu to uncheck all categories/engines.
 * [NEW] Store last used search language in profile.
 * [NEW] Added splitter between search options and search results.
 * [NEW] Added a horizontal splitter in the Categories Manager.
 * [NEW] Stucture/data-type of json response verification, not to strict tho.
 * [NEW] Profile presets (Web, Tor, i2p)
 * [NEW] Make CLI spam settable.
 * [NEW] Stats2 'analytics' filter.
 * [NEW] Find text in search results (Ctrl+F)
 * [NEW] Promt error dialog when updating instances failed.
 * [FIX] Clear search query on profile load.
 * [FIX] Bug where it was possible to create a profile with empty name.
 * [UPT] Changed instance version filter to a minimum version filter.
 * [UPT] Documentation.


## Version 0.2-beta3
 * [FIX] Instance filtering was filtering out all instances when a engine
         filter was set, this due searx-stats2 api changed.


## Version 0.2-beta2
 * [FIX] Crash: "QThread: Destroyed while thread is still running".


## Version 0.2-beta1

 * [NEW] Profiles.
 * [NEW] Custom search categories (engine collections).
 * [NEW] Custom (private?) instances (with user profile type).
 * [NEW] Show results files information (torrent/magnet links, file count,
         file size and peers)
 * [NEW] Customize user-agent strings, with option to pick a random user-
         agent string from the list for every request.
 * [NEW] Application is translatable now. (There are no translations yet
         but it's possible.)
 * [NEW] Whitelist filter instances.
 * [NEW] Keep settings window size persistent.
 * [NEW] Updating instances from stats2 is threaded now.
 * [UPT] Settings window got redesigned with tabs.
 * [UPT] Engines will be checked / unchecked when a category gets checked
         / unchecked.
 * [UPT] Documentation.
 * [FIX] `QLayout: Attempting to add QLayout "" to QWidget "", which
         already has a layout.`
 * [FIX] Keep current instance selected (Instances table) while it exists
         after the table changed.


## Version 0.1-beta2

 * [NEW] Easier creation of Debian package with `python-stdeb`.
 * [NEW] Ability to set the URL of the searx-stats2 instance (found in
         Settings).
 * [FIX] Instances version filter did display all left-over versions in
         the combobox after adding one or more versions to the filter.
 * [UPT] Documentation.

