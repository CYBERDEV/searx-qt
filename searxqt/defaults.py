########################################################################
#  Searx-Qt - Lightweight desktop application for Searx.
#  Copyright (C) 2020-2022  CYBERDEViL
#
#  This file is part of Searx-Qt.
#
#  Searx-Qt is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Searx-Qt is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
########################################################################


from searxqt.core.guard import ConsequenceType
from searxqt.core.requests import ErrorType
from searxqt.core.handler import NetworkTypes


""" This can be used to set default settings for new profiles.

Only what is defined below is supported at the moment. The Presets dict below
is the relevant part.
"""


Guard = {
    "enabled": True,
    "storeLog": True,  # This won't do anything unless enabled is set to True
    "maxLogDays": 7,
    "rules": [
        # The order of this list does mather!

        # Wrong status code (!200)
        # ____________________________________________________________________
        ## 429 - To many requests
        {  # Blacklist 8x status code 429
            'condition': {'errorType': ErrorType.WrongStatus, 'amount': 8, 'period': 0, 'status': 429},
            'consequence': {'type': ConsequenceType.Blacklist, 'duration': 0}
        }, {  # Timeout 4x status code 429 for 1 day
            'condition': {'errorType': ErrorType.WrongStatus, 'amount': 4, 'period': 0, 'status': 429},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 1440}
        }, {  # Timeout 3x status code 429 for 60min
            'condition': {'errorType': ErrorType.WrongStatus, 'amount': 3, 'period': 0, 'status': 429},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 60}

        ## 403 - Forbidden
        }, {  # Blacklist
            'condition': {'errorType': ErrorType.WrongStatus, 'amount': 1, 'period': 0, 'status': 403},
            'consequence': {'type': ConsequenceType.Blacklist, 'duration': 0}

        ## 400 - Bad request
        }, {  # Blacklist 3x status code 400
            'condition': {'errorType': ErrorType.WrongStatus, 'amount': 3, 'period': 0, 'status': 400},
            'consequence': {'type': ConsequenceType.Blacklist, 'duration': 1440}
        }, {  # Timeout 1x status code 400
            'condition': {'errorType': ErrorType.WrongStatus, 'amount': 1, 'period': 0, 'status': 400},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 1440}

        ## Status code catch all other
        }, {  # Timeout 1x status code 429 for 10min
            'condition': {'errorType': ErrorType.WrongStatus, 'amount': 1, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 10}

        # HttpError
        # ____________________________________________________________________
        }, {  # Blacklist 6x error
            'condition': {'errorType': ErrorType.HttpError, 'amount': 6, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Blacklist, 'duration': 0}
        }, {  # Timeout 4x for 1 day
            'condition': {'errorType': ErrorType.HttpError, 'amount': 4, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 1440}
        }, {  # Timeout 1x for 10min
            'condition': {'errorType': ErrorType.HttpError, 'amount': 1, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 30}

        # Timeout
        # ____________________________________________________________________
        }, {  # Blacklist 6x
            'condition': {'errorType': ErrorType.Timeout, 'amount': 6, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Blacklist, 'duration': 0}
        }, {  # Timeout 4x for 1 day
            'condition': {'errorType': ErrorType.Timeout, 'amount': 4, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 1440}
        }, {  # Timeout 3x for 2 hours
            'condition': {'errorType': ErrorType.Timeout, 'amount': 3, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 120}
        }, {  # Timeout 2x for 20min
            'condition': {'errorType': ErrorType.Timeout, 'amount': 2, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 30}
        }, {  # Timeout 1x for 10min
            'condition': {'errorType': ErrorType.Timeout, 'amount': 1, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 10}

        # DecodeError
        # ____________________________________________________________________
        }, {  # Blacklist 6x error
            'condition': {'errorType': ErrorType.DecodeError, 'amount': 6, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Blacklist, 'duration': 0}
        }, {  # Timeout 2x for 1 day
            'condition': {'errorType': ErrorType.DecodeError, 'amount': 2, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 1440}
        }, {  # Timeout 1x for 10min
            'condition': {'errorType': ErrorType.DecodeError, 'amount': 1, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 10}

        # NoResults
        # ____________________________________________________________________
        }, {   # Timeout 4x for 60min.
            'condition': {'errorType': ErrorType.NoResults, 'amount': 4, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 60}
        }, {   # Timeout 3x for 10min.
            'condition': {'errorType': ErrorType.NoResults, 'amount': 3, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 10}
        }, {   # Timeout 2x for 3min.
            'condition': {'errorType': ErrorType.NoResults, 'amount': 2, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 3}
        }, {   # Timeout 1x for 1min.
            'condition': {'errorType': ErrorType.NoResults, 'amount': 1, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 1}

        # SSLError
        # ____________________________________________________________________
        }, {   # Timeout 6x for 1 week.
            'condition': {'errorType': ErrorType.SSLError, 'amount': 6, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 10080}
        }, {   # Timeout 4x for 1 day.
            'condition': {'errorType': ErrorType.SSLError, 'amount': 4, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 1440}
        }, {   # Timeout 3x for 60min.
            'condition': {'errorType': ErrorType.SSLError, 'amount': 3, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 60}
        }, {   # Timeout 1x for 30min.
            'condition': {'errorType': ErrorType.SSLError, 'amount': 1, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 30}

        # InvalidSchema
        # this is probably a bug in Searx-Qt, please report when this happends.
        # ____________________________________________________________________
        }, {   # Timeout until restart
            'condition': {'errorType': ErrorType.InvalidSchema, 'amount': 0, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 0}

        # ConnectionError
        # ____________________________________________________________________
        }, {  # Blacklist 6x
            'condition': {'errorType': ErrorType.ConnectionError, 'amount': 6, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Blacklist, 'duration': 0}
        }, {  # Timeout 2x or 1 day
            'condition': {'errorType': ErrorType.ConnectionError, 'amount': 2, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 1440}
        }, {  # Timeout 1x for 2 hours
            'condition': {'errorType': ErrorType.ConnectionError, 'amount': 1, 'period': 0, 'status': None},
            'consequence': {'type': ConsequenceType.Timeout, 'duration': 120}
        }
    ]
}


InstancesView = {
    # Hide all columns except the instance it's url and version.
    "hiddenColumnIndexes": [2,3,4,5,6,7,8,9,10]
}


Presets = {
    "Web": {
        "guard": Guard,
        "instancesView": InstancesView,
        "instancesFilter": {
            "networkTypes": [NetworkTypes.Web],
            "version": {
                "min": "1.0.0",
                "git": True,
                "dirty": False,
                "extra": False,
                "unknown": False,
                "invalid": False
            },
            "analytics": True
        },
        "settings": {
            "requests": {
                "useragents": [
                    "Mozilla/5.0 (X11; Linux x86_64; rv:121.0) Gecko/20100101 iceweasel/121.0.1",
                    "searx-qt"
                ],
                "extraHeaders": {
                    "Accept": "text/html",
                    "DNT": "1",
                    "Accept-Language": "en-US,en;q=0.5",
                    "Accept-Encoding": "gzip, deflate",
                    "Connection": "keep-alive"
                }
            }
        }
    },

    "Tor": {
        "guard": {
            "enabled": False
        },
        "instancesView": InstancesView,
        "instancesFilter": {
            "networkTypes": [NetworkTypes.Tor],
            "asnPrivacy": False,
            "analytics": True
        },
        "settings": {
            "stats2": {
                "url": "http://searxspbitokayvkhzhsnljde7rqmn7rvoga6e4waeub3h7ug3nghoad.onion/"
            },
            "requests": {
                "useragents": [],
                "kwargs": {
                    "proxies": {
                        "http": "socks5h://127.0.0.1:9050",
                        "https": "socks5h://127.0.0.1:9050"
                    },
                    "timeout": 30
                }
            }
        }
    },

    "i2p": {
        "guard": {
            "enabled": False
        },
        "instancesView": InstancesView,
        "instancesFilter": {
            "networkTypes": [NetworkTypes.I2P],
            "asnPrivacy": False
        },
        "settings": {
            "requests": {
                "useragents": [],
                "kwargs": {
                    "proxies": {
                        "http": "socks5h://127.0.0.1:4444",
                        "https": "socks5h://127.0.0.1:4444"
                    },
                    "timeout": 120
                }
            }
        },
        "data": {
            "instances": {
                "http://ransack.i2p/": {"network_type": NetworkTypes.I2P},
                "http://searx.i2p/": {"network_type": NetworkTypes.I2P},
                "http://mqamk4cfykdvhw5kjez2gnvse56gmnqxn7vkvvbuor4k4j2lbbnq.b32.i2p/":
                    {"network_type": NetworkTypes.I2P}
            }
        }
    }
}
