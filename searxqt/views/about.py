########################################################################
#  Searx-Qt - Lightweight desktop application for Searx.
#  Copyright (C) 2020-2022  CYBERDEViL
#
#  This file is part of Searx-Qt.
#
#  Searx-Qt is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Searx-Qt is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
########################################################################

from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import QT_VERSION_STR
from sys import version as PYTHON_VERSION

from searxqt.version import __version__, __git__
from searxqt.translations import _

__description__ = str(
    "<center><h1>Searx-Qt</h1>"
    "<h3>{0}</h3></center>"
    "<p>{1}: https://notabug.org/CYBERDEViL/searx-qt</p>"
    "<p>{2}: GPL3</p>"
    "<p>{3}: {4}{5}</p>"
    "<p>Python: {5}</p>"
    "<p>PyQt5 : {6}</p>".format(
        _("A lightweight desktop application for Searx"),
        _("Source"),
        _("License"),
        _("Version"),
        __version__,
        f"-{__git__}" if __git__ else "",
        PYTHON_VERSION,
        QT_VERSION_STR
    )
)


def show(parent=None):
    QMessageBox.about(parent, _("About") + " Searx-Qt", __description__)
