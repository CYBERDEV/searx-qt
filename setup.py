#!/usr/bin/env python
from setuptools import setup

from searxqt.version import __version__

with open("README.md", "r") as f:
    long_description = f.read()

setup (
    name="searx-qt",
    version=__version__,
    description="Desktop client application for Searx/SearXNG/searx-space.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://notabug.org/cyberdevil",
    author="CYBERDEViL",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: X11 Applications :: Qt",
        "Topic :: Internet",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
        "Topic :: Internet :: WWW/HTTP"
    ],
    keywords="searx, searxng, client, qt, desktop",
    packages=[
        "searxqt",
        "searxqt.core",
        "searxqt.models",
        "searxqt.views",
        "searxqt.widgets",
        "searxqt.utils"
    ],
    python_requires=">=3.7, <4",
    install_requires=[
        "requests",
        "jsonschema",
        "pyqt5",
        "beautifulsoup4"
    ],
    extras_require={  # Optional
        "socks5": ["pysocks"],
        "images": ["pillow"]
    }
)
