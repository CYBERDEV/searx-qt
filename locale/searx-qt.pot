# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: 0.5.0a1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-27 16:52+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: searxqt/main.py:119
msgid "File"
msgstr ""

#: searxqt/main.py:121 searxqt/views/settings.py:164 searxqt/views/guard.py:476
#: searxqt/widgets/dialogs.py:67
msgid "Save"
msgstr ""

#: searxqt/main.py:126 searxqt/views/profiles.py:226
msgid "Exit"
msgstr ""

#: searxqt/main.py:134 searxqt/views/settings.py:589
msgid "Settings"
msgstr ""

#: searxqt/main.py:139
msgid "Profiles"
msgstr ""

#: searxqt/main.py:144 searxqt/views/about.py:50
msgid "About"
msgstr ""

#: searxqt/main.py:291
msgid "Instances update thread active"
msgstr ""

#: searxqt/main.py:292
msgid ""
"Please wait until instances finished updating before\n"
"switching profiles."
msgstr ""

#: searxqt/translations.py:91
msgid "year"
msgstr ""

#: searxqt/translations.py:91
msgid "years"
msgstr ""

#: searxqt/translations.py:92
msgid "day"
msgstr ""

#: searxqt/translations.py:92
msgid "days"
msgstr ""

#: searxqt/translations.py:93
msgid "hour"
msgstr ""

#: searxqt/translations.py:93
msgid "hours"
msgstr ""

#: searxqt/translations.py:94 searxqt/views/guard.py:430
#: searxqt/views/guard.py:467
msgid "min"
msgstr ""

#: searxqt/translations.py:94
msgid "mins"
msgstr ""

#: searxqt/utils/string.py:33 searxqt/views/search.py:357
#: searxqt/views/instances.py:428 searxqt/views/instances.py:546
#: searxqt/views/profiles.py:320 searxqt/views/profiles.py:374
msgid "Yes"
msgstr ""

#: searxqt/utils/string.py:33 searxqt/views/search.py:358
#: searxqt/views/instances.py:431 searxqt/views/instances.py:547
#: searxqt/views/profiles.py:321 searxqt/views/profiles.py:375
msgid "No"
msgstr ""

#: searxqt/utils/string.py:90
msgid "file"
msgstr ""

#: searxqt/core/htmlGen.py:106
msgid "Suggestions"
msgstr ""

#: searxqt/core/htmlGen.py:114
msgid "Corrections"
msgstr ""

#: searxqt/core/htmlGen.py:134
msgid "Answers"
msgstr ""

#: searxqt/core/htmlGen.py:168
msgid "Unresponsive engines"
msgstr ""

#: searxqt/core/htmlGen.py:228 searxqt/views/search.py:143
msgid "Engines"
msgstr ""

#: searxqt/core/searx.py:493 searxqt/views/settings.py:593
msgid "General"
msgstr ""

#: searxqt/core/searx.py:494
msgid "Files"
msgstr ""

#: searxqt/core/searx.py:495
msgid "Images"
msgstr ""

#: searxqt/core/searx.py:496
msgid "Videos"
msgstr ""

#: searxqt/core/searx.py:497
msgid "IT"
msgstr ""

#: searxqt/core/searx.py:498
msgid "Location"
msgstr ""

#: searxqt/core/searx.py:499
msgid "Music"
msgstr ""

#: searxqt/core/searx.py:500
msgid "News"
msgstr ""

#: searxqt/core/searx.py:501
msgid "Science"
msgstr ""

#: searxqt/core/searx.py:502
msgid "Social"
msgstr ""

#: searxqt/core/searx.py:503
msgid "Onions"
msgstr ""

#: searxqt/core/searx.py:504
msgid "Shopping"
msgstr ""

#: searxqt/core/searx.py:560
msgid "Anytime"
msgstr ""

#: searxqt/core/searx.py:561
msgid "Last day"
msgstr ""

#: searxqt/core/searx.py:562
msgid "Last week"
msgstr ""

#: searxqt/core/searx.py:563
msgid "Last month"
msgstr ""

#: searxqt/core/searx.py:564
msgid "Last year"
msgstr ""

#: searxqt/core/searx.py:569
msgid "No language"
msgstr ""

#: searxqt/core/searx.py:570
msgid "Default language"
msgstr ""

#: searxqt/core/guard.py:142 searxqt/views/instances.py:1058
msgid "Blacklist"
msgstr ""

#: searxqt/core/guard.py:143 searxqt/views/settings.py:300
#: searxqt/views/instances.py:1064
msgid "Timeout"
msgstr ""

#: searxqt/views/settings.py:75
msgid "user:pass@host:port"
msgstr ""

#: searxqt/views/settings.py:77 searxqt/models/instances.py:725
msgid "Enabled"
msgstr ""

#: searxqt/views/settings.py:78
msgid "Proxy DNS"
msgstr ""

#: searxqt/views/settings.py:79
msgid "Protocol"
msgstr ""

#: searxqt/views/settings.py:80
msgid "Address"
msgstr ""

#: searxqt/views/settings.py:122
msgid "Not available for http proxy."
msgstr ""

#: searxqt/views/settings.py:145 searxqt/views/settings.py:168
#: searxqt/views/settings.py:174 searxqt/views/settings.py:416
#: searxqt/views/guard.py:151
msgid "Edit"
msgstr ""

#: searxqt/views/settings.py:148 searxqt/views/search.py:394
#: searxqt/views/instances.py:434 searxqt/views/guard.py:475
#: searxqt/views/profiles.py:116 searxqt/views/profiles.py:224
#: searxqt/widgets/dialogs.py:81
msgid "Cancel"
msgstr ""

#: searxqt/views/settings.py:191
msgid "WARNING! Edit below only when you know what you are doing!"
msgstr ""

#: searxqt/views/settings.py:250
msgid "Random"
msgstr ""

#: searxqt/views/settings.py:293
msgid "Verify"
msgstr ""

#: searxqt/views/settings.py:304
msgid "Proxy"
msgstr ""

#: searxqt/views/settings.py:309
msgid "User-Agents"
msgstr ""

#: searxqt/views/settings.py:313
msgid "Extra headers"
msgstr ""

#: searxqt/views/settings.py:349
msgid ""
"Since many SearXNG instances block API requests we now have a option to not "
"use it and parse HTML response instead. Check \"Parse HTML\" below for that, "
"when left unchecked it will use the API instead."
msgstr ""

#: searxqt/views/settings.py:358
msgid "Parse HTML"
msgstr ""

#: searxqt/views/settings.py:363
msgid "Enable \"Safe search\" for search engines that support it."
msgstr ""

#: searxqt/views/settings.py:368
msgid "Safe Search"
msgstr ""

#: searxqt/views/settings.py:397
msgid ""
"The Searx-Stats2 project lists public Searx instances with statistics. The "
"original instance is running at https://searx.space/. This is where Searx-Qt "
"will request a list with instances when the update button is pressed."
msgstr ""

#: searxqt/views/settings.py:417
msgid "Reset"
msgstr ""

#: searxqt/views/settings.py:453
msgid "<h2>CLI output level</h2>"
msgstr ""

#: searxqt/views/settings.py:454
msgid "Info"
msgstr ""

#: searxqt/views/settings.py:455
msgid "Warning"
msgstr ""

#: searxqt/views/settings.py:456
msgid "Debug"
msgstr ""

#: searxqt/views/settings.py:457 searxqt/widgets/dialogs.py:44
msgid "Error"
msgstr ""

#: searxqt/views/settings.py:474
msgid ""
"Debug mode enabled via environment variable 'SEARXQT_DEBUG'. The settings "
"below are ignored, unset 'SEARXQT_DEBUG' and restart Searx-Qt to disable "
"debug mode."
msgstr ""

#: searxqt/views/settings.py:538
msgid "Theme:"
msgstr ""

#: searxqt/views/settings.py:557
msgid "Base style:"
msgstr ""

#: searxqt/views/settings.py:597
msgid "Connection"
msgstr ""

#: searxqt/views/settings.py:610
msgid "Guard"
msgstr ""

#: searxqt/views/search.py:201
msgid "Category manager"
msgstr ""

#: searxqt/views/search.py:240 searxqt/views/guard.py:442
#: searxqt/widgets/buttons.py:121
msgid "All"
msgstr ""

#: searxqt/views/search.py:246
msgid "Category"
msgstr ""

#: searxqt/views/search.py:349
msgid "Delete category"
msgstr ""

#: searxqt/views/search.py:352
msgid "Are you sure you want to delete the "
msgstr ""

#: searxqt/views/search.py:386 searxqt/views/profiles.py:86
#: searxqt/models/instances.py:726
msgid "Name"
msgstr ""

#: searxqt/views/search.py:392
msgid "My category"
msgstr ""

#: searxqt/views/search.py:395 searxqt/views/instances.py:64
#: searxqt/views/guard.py:150 searxqt/views/profiles.py:112
#: searxqt/views/profiles.py:200
msgid "Add"
msgstr ""

#: searxqt/views/search.py:455 searxqt/models/instances.py:727
msgid "Categories"
msgstr ""

#: searxqt/views/search.py:492
msgid "Custom"
msgstr ""

#: searxqt/views/search.py:495
msgid "Manage"
msgstr ""

#: searxqt/views/search.py:516
msgid "Instances"
msgstr ""

#: searxqt/views/search.py:533
msgid "Default"
msgstr ""

#: searxqt/views/search.py:1042
msgid "Show / Hide"
msgstr ""

#: searxqt/views/search.py:1079
msgid "Find .."
msgstr ""

#: searxqt/views/search.py:1082
msgid "Case sensitive"
msgstr ""

#: searxqt/views/search.py:1083
msgid "Whole words"
msgstr ""

#: searxqt/views/search.py:1185
msgid "Search for .."
msgstr ""

#: searxqt/views/search.py:1188
msgid "Searx"
msgstr ""

#: searxqt/views/search.py:1189
msgid "Preform search."
msgstr ""

#: searxqt/views/search.py:1193
msgid "Reload"
msgstr ""

#: searxqt/views/search.py:1198
msgid ""
"Search with random instance.\n"
"(Obsolete when 'Random Every is checked')"
msgstr ""

#: searxqt/views/search.py:1207
msgid "Fallback"
msgstr ""

#: searxqt/views/search.py:1208
msgid "Try random other instance on fail."
msgstr ""

#: searxqt/views/search.py:1211
msgid "Random every"
msgstr ""

#: searxqt/views/search.py:1212
msgid "Pick a random instance for every request."
msgstr ""

#: searxqt/views/search.py:1399
msgid "Search"
msgstr ""

#: searxqt/views/search.py:1407
msgid "Stop"
msgstr ""

#: searxqt/views/search.py:1444
msgid "Please enter a search query."
msgstr ""

#: searxqt/views/search.py:1448
msgid "Please select a instance first."
msgstr ""

#: searxqt/views/search.py:1548
msgid "Search request failed."
msgstr ""

#: searxqt/views/about.py:37
msgid "A lightweight desktop application for Searx"
msgstr ""

#: searxqt/views/about.py:38
msgid "Source"
msgstr ""

#: searxqt/views/about.py:39
msgid "License"
msgstr ""

#: searxqt/views/about.py:40 searxqt/views/instances.py:1055
msgid "Version"
msgstr ""

#: searxqt/views/instances.py:163
msgid "Update"
msgstr ""

#: searxqt/views/instances.py:166
msgid "Add Instance"
msgstr ""

#: searxqt/views/instances.py:346
msgid "Add to blacklist"
msgstr ""

#: searxqt/views/instances.py:349
msgid "Temporary blacklist"
msgstr ""

#: searxqt/views/instances.py:352
msgid "Add to whitelist"
msgstr ""

#: searxqt/views/instances.py:357
msgid "Copy column text"
msgstr ""

#: searxqt/views/instances.py:365
msgid "Copy JSON data"
msgstr ""

#: searxqt/views/instances.py:369
msgid "Select All"
msgstr ""

#: searxqt/views/instances.py:373
msgid "Columns"
msgstr ""

#: searxqt/views/instances.py:385
msgid "Remove selected"
msgstr ""

#: searxqt/views/instances.py:386
msgid "Update selected"
msgstr ""

#: searxqt/views/instances.py:438
#, python-brace-format
msgid ""
"{url} found in the <b>whitelist</b>. Would you like to <b>remove</b> it from "
"the <b>whitelist</b> and add it to the <b>blacklist</b>?"
msgstr ""

#: searxqt/views/instances.py:445
msgid "Remember for all"
msgstr ""

#: searxqt/views/instances.py:466 searxqt/views/instances.py:474
msgid "Manual"
msgstr ""

#: searxqt/views/instances.py:537
msgid "Delete instances"
msgstr ""

#: searxqt/views/instances.py:540
msgid "Are you sure you want to delete the following instances?\n"
msgstr ""

#: searxqt/views/instances.py:717
msgid "Until restart or manual removal."
msgstr ""

#: searxqt/views/instances.py:832
msgid "Minimum:"
msgstr ""

#: searxqt/views/instances.py:838
msgid "Invalid"
msgstr ""

#: searxqt/views/instances.py:840
msgid "Include instances with a invalid version"
msgstr ""

#: searxqt/views/instances.py:843
msgid "Development"
msgstr ""

#: searxqt/views/instances.py:845
msgid "Include development versions of SearX/SearXNG (git versions)."
msgstr ""

#: searxqt/views/instances.py:848
msgid "Dirty"
msgstr ""

#: searxqt/views/instances.py:850
msgid "Include SearXNG git versions with uncommited changes."
msgstr ""

#: searxqt/views/instances.py:855
msgid "Extra"
msgstr ""

#: searxqt/views/instances.py:857
msgid "Include versions flagged as extra (IDK what that is? TODO)."
msgstr ""

#: searxqt/views/instances.py:862
msgid "Unknown"
msgstr ""

#: searxqt/views/instances.py:864
msgid "Include Searx versions with unknown git commit."
msgstr ""

#: searxqt/views/instances.py:936
msgid "Off"
msgstr ""

#: searxqt/views/instances.py:1045
msgid "<h3>Filter</h3>"
msgstr ""

#: searxqt/views/instances.py:1048
msgid "Network"
msgstr ""

#: searxqt/views/instances.py:1061
msgid "Whitelist"
msgstr ""

#: searxqt/views/instances.py:1077
msgid "Require ASN privacy"
msgstr ""

#: searxqt/views/instances.py:1080
msgid ""
"Filter out instances that run their server at a known\n"
"malicious network like Google, CloudFlare, Akamai etc..\n"
"\n"
"This does not give any guarantee, it only filters known\n"
"privacy violators!"
msgstr ""

#: searxqt/views/instances.py:1090
msgid "Require IPv6"
msgstr ""

#: searxqt/views/instances.py:1093
msgid "Only instances with at least one ipv6-address remain if checked."
msgstr ""

#: searxqt/views/instances.py:1104
msgid "Excl. analytics"
msgstr ""

#: searxqt/views/instances.py:1107
msgid "Exclude instances running tracking software."
msgstr ""

#: searxqt/views/instances.py:1149
msgid "<h3>Stats</h3>"
msgstr ""

#: searxqt/views/instances.py:1168 searxqt/widgets/buttons.py:238
msgid "Hide"
msgstr ""

#: searxqt/views/instances.py:1173
msgid "Total count"
msgstr ""

#: searxqt/views/instances.py:1176
msgid "After filter count"
msgstr ""

#: searxqt/views/instances.py:1180
msgid "Last update"
msgstr ""

#: searxqt/views/instances.py:1191
msgid "Show"
msgstr ""

#: searxqt/views/guard.py:68
msgid ""
"Guard can put failing instances on the blacklist or on a timeout based on "
"set rules below."
msgstr ""

#: searxqt/views/guard.py:76
msgid "Enable guard"
msgstr ""

#: searxqt/views/guard.py:80
msgid "Store log"
msgstr ""

#: searxqt/views/guard.py:84
msgid " days"
msgstr ""

#: searxqt/views/guard.py:98
msgid "Rules"
msgstr ""

#: searxqt/views/guard.py:102
msgid "Log"
msgstr ""

#: searxqt/views/guard.py:123 searxqt/views/guard.py:611
msgid "Clear log?"
msgstr ""

#: searxqt/views/guard.py:124
msgid ""
"You've disabled Guard but there are currently logs present. Do you want to "
"clear the log?"
msgstr ""

#: searxqt/views/guard.py:153
msgid "Del"
msgstr ""

#: searxqt/views/guard.py:156
msgid "Move rule up."
msgstr ""

#: searxqt/views/guard.py:158
msgid "Move rule down."
msgstr ""

#: searxqt/views/guard.py:324
msgid "Always"
msgstr ""

#: searxqt/views/guard.py:346
msgid "Until restart"
msgstr ""

#: searxqt/views/guard.py:398
msgid "Guard rule editor"
msgstr ""

#: searxqt/views/guard.py:418
msgid "Error type"
msgstr ""

#: searxqt/views/guard.py:424
msgid "Amount of failes to trigger"
msgstr ""

#: searxqt/views/guard.py:434
msgid "Timeframe in minutes where the amount of failes have to occur in"
msgstr ""

#: searxqt/views/guard.py:451
msgid "Response status code"
msgstr ""

#: searxqt/views/guard.py:461
msgid "Destination"
msgstr ""

#: searxqt/views/guard.py:470
msgid "Duration of the timeout"
msgstr ""

#: searxqt/views/guard.py:548
msgid "Refresh"
msgstr ""

#: searxqt/views/guard.py:551
msgid "Clear"
msgstr ""

#: searxqt/views/guard.py:576
msgid "Copy URL"
msgstr ""

#: searxqt/views/guard.py:579
msgid "Clear log for selected instance(s)"
msgstr ""

#: searxqt/views/guard.py:610
msgid "Confirm clear log"
msgstr ""

#: searxqt/views/profiles.py:50
msgid "Add profile"
msgstr ""

#: searxqt/views/profiles.py:58
msgid ""
"There are two types of profiles:\n"
"  1. stats2 profile\n"
"  2. user profile\n"
"\n"
"Choose stats2 profile type when you want to get a\n"
"list of searx-instances from a stats2-instance.\n"
"For example from the default https://searx.space\n"
"instance.\n"
"Choose user profile type when you want to manage\n"
"your own searx-instance's list."
msgstr ""

#: searxqt/views/profiles.py:78
msgid "Type"
msgstr ""

#: searxqt/views/profiles.py:102
msgid "Preset"
msgstr ""

#: searxqt/views/profiles.py:155
msgid "Name already exists."
msgstr ""

#: searxqt/views/profiles.py:167
msgid "Profile select"
msgstr ""

#: searxqt/views/profiles.py:189
msgid "Please select a profile"
msgstr ""

#: searxqt/views/profiles.py:203
msgid "Delete"
msgstr ""

#: searxqt/views/profiles.py:207
msgid ""
"Force\n"
"release\n"
"profiles"
msgstr ""

#: searxqt/views/profiles.py:219
msgid "Use as default"
msgstr ""

#: searxqt/views/profiles.py:230
msgid "Load profile"
msgstr ""

#: searxqt/views/profiles.py:263
msgid "Failed to load profile."
msgstr ""

#: searxqt/views/profiles.py:276
msgid "Profile already in use."
msgstr ""

#: searxqt/views/profiles.py:285
msgid "Profile doesn't exist anymore."
msgstr ""

#: searxqt/views/profiles.py:316
#, python-brace-format
msgid "Delete profile {profile.name}"
msgstr ""

#: searxqt/views/profiles.py:317
msgid "Are you sure you want to delete profile "
msgstr ""

#: searxqt/views/profiles.py:329
msgid "Failed to delete profile."
msgstr ""

#: searxqt/views/profiles.py:330
msgid "Cannot remove active profiles!"
msgstr ""

#: searxqt/views/profiles.py:361
msgid "Force release active profiles"
msgstr ""

#: searxqt/views/profiles.py:363
msgid ""
"This will force release all active profiles.\n"
"\n"
"This should only be run if Searx-Qt didn't exit properly\n"
"and you can't access your profile anymore.\n"
"\n"
"Make sure to close all other running Searx-Qt instances\n"
"before continuing!\n"
"\n"
"Do you want to continue?"
msgstr ""

#: searxqt/widgets/buttons.py:231
msgid "Uncheck all"
msgstr ""

#: searxqt/widgets/dialogs.py:47
msgid "A error has occured!"
msgstr ""

#: searxqt/widgets/dialogs.py:49
msgid "Ok"
msgstr ""

#: searxqt/models/search.py:322
#, python-brace-format
msgid "<b>Updating data:</b> {url} ({queueCount} left)"
msgstr ""

#: searxqt/models/instances.py:123
#, python-brace-format
msgid "<b>Updating data from:</b> {url}"
msgstr ""

#: searxqt/models/instances.py:728
msgid "Language support"
msgstr ""

#: searxqt/models/instances.py:729
msgid "Paging"
msgstr ""

#: searxqt/models/instances.py:730
msgid "SafeSearch"
msgstr ""

#: searxqt/models/instances.py:731
msgid "Shortcut"
msgstr ""

#: searxqt/models/instances.py:732
msgid "Time-range support"
msgstr ""
